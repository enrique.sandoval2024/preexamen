/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

/**
 *
 * @author enriq
 */
public class Registro {
    private int codVenta;
    private int cantidad;
    private int tipo;
    private float precio;
    
    public Registro() {
    
        this.cantidad = 0;
        this.codVenta = 0;
        this.tipo = 0;
        this.precio = 0.0f;
}

    public Registro(int codVenta, int cantidad, int tipo, float precio) {
        this.codVenta = codVenta;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }
    
    public Registro(Registro otro){
        this.codVenta = otro.codVenta;
        this.cantidad = otro.cantidad;
        this.tipo = otro.tipo;
        this.precio = otro.precio;
    }

    public int getCodVenta() {
        return codVenta;
    }

    public void setCodVenta(int codVenta) {
        this.codVenta = codVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    public float calcularCostoVenta(){
        float costoVenta=0.0f;
        costoVenta = this.precio * this.cantidad;
        return costoVenta;
    }
    public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto = this.calcularCostoVenta() * 0.16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float totalPagar = 0.0f;
        totalPagar = this.calcularImpuesto() + this.calcularCostoVenta();
        return totalPagar;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
